{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}

module Main where

import Network.Wai.Handler.Warp
import Servant (serve)
import Database.PostgreSQL.Simple
import Crypto.Saltine
import Crypto.Saltine.Core.Box (Keypair, newKeypair)
import Types
import Api

main :: IO ()
main = do
  conn <- connectPostgreSQL "host=localhost port=5432 user=postgres"
  sodiumInit
  k <- newKeypair
  runApp conn k

runApp :: Connection -> Keypair -> IO ()
runApp conn k = run 8080 (serve api $ server conn k)
