# servant-chat

A HTTP "chat" demo on Servant, postgresql-simple and Saltine.

## Running

```sh
1$ nix-shell
1$ cabal --disable-nix run servant-chat-exe # starts a server on port 8080

2$ nix-shell
2$ curl localhost:8080/send?message=moin # send a message
2$ cabal --disable-nix exec servant-chat-client-exe # see log
```
