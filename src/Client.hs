{-# LANGUAGE DataKinds #-}

module Client where

import Servant
import Servant.Client
import Types

send :<|> receive :<|> chat = client api
